module.exports = {
  apps: [
    {
      name: 'iLotusland_watchman_FTP_Watch_TXT',
      script: './watch-txt.js',
      env: {
        'NODE_PATH': './'
      }
    }, {
      name: 'iLotusland_watchman_FTP_Watch_CSV',
      script: './watch-csv.js',
      env: {
        'NODE_PATH': './'
      }
    }]
}

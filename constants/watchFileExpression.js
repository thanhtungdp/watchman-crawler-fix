const config = require('../config')

module.exports = config.IS_CSV
  ? ['anyof', ['match', '*.txt'], ['match', '*.csv']]
  : ['anyof', ['match', '*.txt']]

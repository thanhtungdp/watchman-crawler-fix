require('dotenv').config()

module.exports = {
  TYPE: process.env.TYPE,
  ROOT_FTP: process.env.ROOT_FTP,
  ROOT_CSV: process.env.ROOT_CSV,
  ROOT_FTP_ERRORS: process.env.ROOT_FTP_ERRORS,
  ROOT_FTP_IMPORTED: process.env.ROOT_FTP_IMPORTED,
  ROOT_CSV_IMPORTED: process.env.ROOT_CSV_IMPORTED,
  WATCHMAN_PATH: process.env.WATCHMAN_PATH,
  CATEGORY_API: process.env.CATEGORY_API,
  IS_CSV: process.env.IS_CSV,
  IS_WINDOW: process.env.IS_WINDOW,
  MONGODB_ADDRESS: process.env.MONGODB_ADDRESS,
  MONGODB_OPTIONS: {
    database: process.env.MONGODB_URL,
    db_options: {
      native_parser: true,
      poolSize: 5,
      user: process.env.MONGODB_USER,
      pass: process.env.MONGODB_PASS,
      promiseLibrary: require('bluebird'),
      autoReconnect: true
    }
  }
}

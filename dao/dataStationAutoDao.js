module.exports = {
  /**
   * Create Data
   * @param key: String,
   * name: Object,
   * stationTypeId: String,
   * address: String,
   * mapLocation: Object, emails: Object, phones: Object, options: Object, measuringList: Object, image: String
   * @return <DataStationAuto>
   */
  async createOrUpdate ({ receivedAt, measuringLogs }, Model) {
    const item = await Model.findOneAndUpdate(
      { receivedAt },
      {
        $set: {
          receivedAt,
          measuringLogs,
          createdAt: Date.now(),
          updatedAt: Date.now()
        }
      },
      { upsert: true, new: true, runValidators: true }
    )
    return item
  }
}

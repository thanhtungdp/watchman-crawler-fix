const path = require('path')
const fs = require('fs')
const { manageConnectDatabase } = require('./lib/db')
const createQueue = require('./lib/queue')
const { getAllStations } = require('./lib/organization/getAllStations')
const sv4ReadAndWriteToServer = require('./lib/sv24/readFile')
const watchman = require('./lib/watchman')
const config = require('./config')
const logger = require('./logger')

const q = createQueue()

/**
 * Watch FTP Folder any file change
 * If have file - auto insert data by station
 */
async function watchFtpPath (ftpRoot, stations, warningConfigs) {
  const fullPath = path.join(ftpRoot)
  const isExists = fs.existsSync(fullPath)

  if (!isExists) {
    logger.error('WATCHMAN: not exists folder ' + ftpRoot)
    return
  }

  const subscription = config.TYPE === 'initial' ? watchman.subscriptionInitial : watchman.subscriptionChange

  watchman.watch(fullPath, (client, resp) => {
    subscription(client, resp.watch, resp.relative_path, async (file) => {
      // Find station matched `file.name` vs `station.path`

      const fileNames = file.name.split('/')
      const fileLastName = fileNames[fileNames.length - 1]

      const station = stations.find(station => {
        return fileLastName.indexOf(station.fileName) > -1
      })

      // Error if file not match with station.path config
      if (!station) {
        logger.error('FOLDER: Not find folder', fileLastName)
        return
      }

      // Return if file delete
      if (!file.exists || file.name.indexOf('IMPORTED') > -1) {
        return
      }

      // Get realfile pathname, combine ROOT_FTP, station.path, filename
      const realFilePath = path.join(ftpRoot, file.name)

      // Add process to queue
      q.push({
        func: sv4ReadAndWriteToServer,
        params: {
          filename: realFilePath,
          organization: station.organization,
          station,
          warningConfigs,
          q
          // isAwait: true
        }
      })
    })
  })
}

async function main (ftpRoot) {
  await manageConnectDatabase()

  try {
    let { stations: dataStations, warningConfigs } = await getAllStations()

    console.log('START INIT: ', ftpRoot)
    watchFtpPath(ftpRoot, dataStations, warningConfigs)

    process.on('exit', () => {
      console.log('exists')
    })

    process.on('SIGINT', () => {
    })
  } catch (e) {
    console.log(e)
  }
}

module.exports = main

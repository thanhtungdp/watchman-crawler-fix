const watchman = require('./lib/watchman')
const config = require('./config')

watchman.watch(config.ROOT_CSV, (client, resp) => {
  watchman.subscriptionInitial(client, resp.watch, resp.relative_path, (file) => {
    console.log(file)
  })
})

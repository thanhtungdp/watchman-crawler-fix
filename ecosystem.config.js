module.exports = {
  apps: [
    {
      name: 'iLotusland_watchman_FTP',
      script: './index-txt.js',
      env: {
        'NODE_PATH': './',
        'TYPE': 'change'
      }
    }, {
      name: 'iLotusland_watchman_CSV',
      script: './index-csv.js',
      env: {
        'NODE_PATH': './',
        'TYPE': 'change'
      }
    }]
}

const watchman = require('./lib/watchman')
const config = require('./config')

watchman.watch(config.ROOT_CSV, (client, resp) => {
  console.log(config.ROOT_CSV)
  watchman.subscriptionChange(client, resp.watch, resp.relative_path, (file) => {
    console.log(file)
  })
})

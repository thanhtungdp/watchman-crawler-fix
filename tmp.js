const createQueue = require('./lib/queue')

const q = createQueue()

let results = []

q.push({ func: () => {
  results.push(2)
},
params: {} })

q.push({ func: () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      results.push(1)
      resolve()
    }, 1000)
  })
},
params: {} })

q.push({ func: () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      results.push(2)
      resolve()
    }, 1000)
  })
},
params: {} })

q.push({ func: () => {
  results.push(3)
},
params: {} })

// q.push(() => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() =>{
//       results.push(1)
//       resolve()
//     }, 100)
//   })

// })

q.on('task_finish', function (taskId, result, stats) {
  console.log(taskId)
  console.log(results)
  // taskId = 1, result: 3, stats = { elapsed: <time taken> }
  // taskId = 2, result: 5, stats = { elapsed: <time taken> }
})

// q.start(() => {
//   console.log(results)
// })

const mongoose = require('mongoose')
const logger = require('../../logger')
/**
 * Get connection from socket history
 * @param dbName
 * @returns {*}
 */
function getConnection (dbInfo) {
  return mongoose.connections.find(connection => {
    return (
      connection.name === dbInfo.name &&
      connection.host === dbInfo.address &&
      connection.user === dbInfo.user &&
      connection.pass === dbInfo.pwd &&
      connection.port.toString() === dbInfo.port.toString()
    )
  })
}

/**
 * Db options to connect
 * @param dbInfo
 * @returns {{urlPath: string, options: {native_parser: boolean, poolSize: number, user, pass, useMongoClient: boolean, promiseLibrary}}}
 */
function getOptionsDb (dbInfo) {
  return {
    urlPath: `mongodb://${dbInfo.address}:${dbInfo.port}/${dbInfo.name}`,
    options: {
      native_parser: true,
      poolSize: 5,
      user: dbInfo.user,
      pass: dbInfo.pwd,
      promiseLibrary: global.Promise,
      connectTimeoutMS: 1200
    }
  }
}

/**
 * Create connection new or old
 * @param dbInfo
 * @returns {*}
 */
function createConnection (dbInfo) {
  const dbConnection = getConnection(dbInfo)
  if (dbConnection) {
    return dbConnection
  }
  logger.info('create new connect')
  const dbOptions = getOptionsDb(dbInfo)
  return mongoose.createConnection(dbOptions.urlPath, dbOptions.options)
}

/**
 * Create Model with connection
 * @param conn
 * @param modelName
 * @param ModelSchema
 */
function createModel (conn, modelName, ModelSchema) {
  return conn.model(modelName, ModelSchema)
}

/**
 * Inject model to dao
 * @param conn
 * @param Dao
 * @returns {*}
 */
function createDao (conn, Dao) {
  return new Dao(createModel(conn, Dao.model.name, Dao.model.schema))
}

/**
 * Assign connection to dao, model
 * @param objectDaos
 * @param mongoDbConnect
 * @returns {{}}
 */
function assignDao (objectDaos, mongoDbConnect) {
  let dao = {}
  Object.keys(objectDaos).forEach(objectKey => {
    if (objectDaos[objectKey]) {
      dao[objectKey] = createDao(mongoDbConnect, objectDaos[objectKey])
    }
  })
  return dao
}

module.exports.getConnection = getConnection
module.exports.createConnection = createConnection
module.exports.createModel = createModel
module.exports.createDao = createDao
module.exports.assignDao = assignDao

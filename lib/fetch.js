const axios = require('axios')

const getHeaders = (token, organizationId) => {
  let headers = {
    Accept: 'application/json'
  }
  if (token) headers.Authorization = token
  if (organizationId) headers.organization = organizationId
  return headers
}

module.exports.putFetch = function putFetch (url, data, props, token) {
  let attributes = Object.assign(
    {
      cache: true,
      headers: getHeaders(token)
    },
    props
  )

  return new Promise((resolve, reject) => {
    axios
      .put(url, data, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject(new Error('putFetch, url:' + url))
        }
      })
      .catch(e => reject(e))
  })
}

module.exports.postFetch = function postFetch (url, params, header = {}, token, organizationId) {
  const headers = Object.assign(getHeaders(token, organizationId), header)
  return new Promise((resolve, reject) => {
    axios
      .post(url, params, {
        cache: true,
        headers
      })
      .then(res => {
        resolve(res.data)
      })
      .catch(e => reject(e))
  })
}

module.exports.getFetch = function getFetch (url, data, props, token, organizationId) {
  let attributes = Object.assign(
    {
      headers: getHeaders(token, organizationId)
    },
    props
  )
  return new Promise((resolve, reject) => {
    axios
      .get(url, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject(new Error('getFetch, url:' + url))
        }
      })
      .catch(e => reject(e))
  })
}

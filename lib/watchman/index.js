const makeWatchFolder = require('./makeWatchFolder')
const makeSubscriptionChange = require('./makeSubscriptionChange')
const makeSubscriptionInitial = require('./makeSubscriptionInitial')

module.exports = {
  watch: makeWatchFolder,
  subscriptionChange: makeSubscriptionChange,
  subscriptionInitial: makeSubscriptionInitial
}

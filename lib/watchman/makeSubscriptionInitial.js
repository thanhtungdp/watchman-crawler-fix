const watchFileExpression = require('../../constants/watchFileExpression')

module.exports = (client, watch, relativePath, callback) => {
  let sub = {
    // Match any `.js` file in the dir_of_interest
    expression: watchFileExpression,
    // suffix: ["txt"],
    // Which fields we're interested in
    fields: ['name', 'size', 'mtime_ms', 'exists', 'type']
  }

  if (relativePath) {
    sub.relative_root = relativePath
  }

  client.command(['subscribe', watch, 'mysubscription', sub],
    function (error, resp) {
      if (error) {
        // Probably an error in the subscription criteria
        console.error('failed to subscribe: ', error)
        return
      }
      console.log('subscription ' + resp.subscribe + ' established')
    })

  // Subscription results are emitted via the subscription event.
  // Note that this emits for all subscriptions.  If you have
  // subscriptions with different `fields` you will need to check
  // the subscription name and handle the differing data accordingly.
  // `resp`  looks like this in practice:
  //
  // { root: '/private/tmp/foo',
  //   subscription: 'mysubscription',
  //   files: [ { name: 'node_modules/fb-watchman/index.js',
  //       size: 4768,
  //       exists: true,
  //       type: 'f' } ] }
  client.on('subscription', function (resp) {
    if (resp.subscription !== 'mysubscription') return

    resp.files.forEach(function (file) {
      // convert Int64 instance to javascript integer
      // const mtimeMs = +file.mtime_ms

      // console.log('file changed: ' + file.name, mtimeMs)

      if (callback) {
        callback(file)
      }
    })
  })
}

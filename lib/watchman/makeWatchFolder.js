const watchman = require('fb-watchman')
const path = require('path')
const config = require('../../config')

const watchmanBinaryPath = !config.IS_WINDOW
  ? 'watchman'
  : path.join(__dirname, '../../', 'watchman-window/watchman.exe')

console.log('WATCHMAN PATH, ', watchmanBinaryPath)

var client = new watchman.Client({
  watchmanBinaryPath
})

module.exports.watchmanBinaryPath = watchmanBinaryPath
module.exports = (DIR_OF_INTEREST, callback) => {
  client.command(['watch-project', DIR_OF_INTEREST],
    function (error, resp) {
      if (error) {
        console.error('Error initiating watch:', error)
        return
      }

      // It is considered to be best practice to show any 'warning' or
      // 'error' information to the user, as it may suggest steps
      // for remediation
      if ('warning' in resp) {
        console.log('warning: ', resp.warning)
      }

      // `watch-project` can consolidate the watch for your
      // dir_of_interest with another watch at a higher level in the
      // tree, so it is very important to record the `relative_path`
      // returned in resp

      // console.log(resp)

      console.log('watch established on ', resp.watch,
        ' relative_path', resp.relative_path)

      if (callback) {
        callback(client, resp)
      }
    })
}

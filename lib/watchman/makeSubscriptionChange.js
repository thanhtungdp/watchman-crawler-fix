const watchFileExpression = require('../../constants/watchFileExpression')

console.log(watchFileExpression)

module.exports = function makeSubscription (client, watch, relativePath, callback = () => { }) {
  client.command(['clock', watch], function (error, resp) {
    if (error) {
      console.error('Failed to query clock:', error)
      return
    }

    const sub = {
      // Match any `.js` file in the dir_of_interest
      expression: watchFileExpression,
      // suffix: ["txt"],
      // Which fields we're interested in
      fields: ['name', 'size', 'exists', 'type'],
      // add our time constraint
      since: resp.clock
    }

    if (relativePath) {
      sub.relative_root = relativePath
    }

    client.command(['subscribe', watch, 'mysubscription', sub],
      function (error, resp) {
        if (error) {
          console.log('Failed to subsribe', error)
          return
        }
        // handle the result here
        console.log(resp)
      })

    client.on('subscription', function (resp) {
      if (resp.subscription !== 'mysubscription') return

      resp.files.forEach(function (file) {
        // convert Int64 instance to javascript integer
        // const mtime_ms = +file.mtime_ms;

        // console.log('file changed: ' + file.name, mtime_ms);

        if (callback) {
          callback(file)
        }
      })
    })
  })
}

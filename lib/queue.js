const Queue = require('better-queue')
// const config = require('../config')

module.exports = function createQueue () {
  const qChild = new Queue(
    async function (inputChild, cbChild) {
      const { func, params } = inputChild
      if (params.isAwait) {
        await func(params)
        cbChild(null)
      } else {
        func(params)
        // if(result && typeof result.then === "function"){
        //   result.then(() => {
        //     cbChild(null)
        //   })
        // } else {
        cbChild(null)
        // }
      }
    },
    {
      // afterProcessDelay: config.QUEUE_TOTAL_READ_CHILD,
      maxRetries: 100,
      retryDelay: 300
    }
  )

  return qChild

  // const queue = new Queue(
  //   async function (input, cb) {
  //     for (let i = 0; i < input.length; i++) {
  //       qChild.push(input[i])
  //       // const { func, params } = input[i]
  //       // func(params)
  //     }
  //     cb(null)
  //   },
  //   {
  //     batchSize: config.QUEUE_TOTAL_READ,
  //     batchDelayTimeout: 10000,
  //     afterProcessDelay: 60
  //   }
  // )
  // return queue
}

const moveFile = require('../sv24/moveFile')
const fs = require('fs')
const _ = require('lodash')
const moment = require('moment')
const logger = require('logger')

module.exports = async ({ filename, showLog }) => {
  try {
    const data = await fs.readFileSync(filename, 'utf8')
    let receivedAt = ''
    const measureData = _.slice(data.split('\n'), 2, data.length)
      .map(row => {
        if (row !== '') {
          let rowValues = row.replace(`/[^\w\s]/gi`, '').replace(/\t/g, ';').replace(/,/g, '').split(';')
          const receivedTime = moment(rowValues[1], 'YYYYMMDDHHmmss')
          if (!receivedTime.isValid()) {
            //moveFile({ filename, type: 'error' })
            throw new Error('Time not valid')
          }

          if (receivedAt === '') {
            receivedAt = receivedTime.toDate()
          }
          return {
            key: receivedTime.format('YYYYMMDDHHmmss'),
            measureName: _.last(rowValues[0].split('.')),
            value: _.isNumber(rowValues[2])
              ? null
              : _.toNumber(rowValues[2]),
            time: receivedTime.toDate(),
            statusDevice: _.isNaN(_.toNumber(rowValues[3])) ? 0 : _.toNumber(rowValues[3])
          }
        }
        return null
      })
      .filter(row => !!row)

    if (showLog) {
      logger.info(`${filename} readed`)
    }
    if (!receivedAt) {
      //moveFile({ filename, type: 'error' })
      logger.error(`${filename} Time not valid`)
      return { error: true }
    }
    return {
      csv: true,
      measureData,
      receivedAt
    }
  } catch (e) {
    //moveFile({ filename, type: 'error' })
    logger.error(`${filename} not read\n${e.message}`)
    return { error: true }
  }
}

const path = require('path')
const fs = require('fs')
const config = require('../../config')

module.exports = async function moveFile ({ filename, type }) {
  function move (dirPath) {
    // move file
    let pathFile = filename.replace(config.ROOT_FTP, '').replace(config.ROOT_CSV, '')
    let newImportPath = `${dirPath}${pathFile}`

    // get root folder import
    const treeImportReal = newImportPath.replace(path.basename(filename), '')

    if (!fs.existsSync(treeImportReal)) {
      let paths = pathFile.split('/')
      let fullPath = dirPath
      paths.forEach(path => {
        if (path.indexOf('.txt') === -1) {
          fullPath = fullPath + '/' + path
          if (!fs.existsSync(fullPath)) {
            fs.mkdirSync(fullPath)
          }
        }
      })
    }
    fs.renameSync(filename, newImportPath)
  }
  switch (type) {
    case 'error':
      move(config.ROOT_FTP_ERRORS)
      break
    case 'imported':
      move(config.ROOT_FTP_IMPORTED)
      break
    case 'csv_imported':
      move(config.ROOT_CSV_IMPORTED)
      break
  }
}

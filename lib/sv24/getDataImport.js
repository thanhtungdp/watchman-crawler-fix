const moment = require('moment')
const _ = require('lodash')
const warningLevels = require('../../constants/warningLevels.js')

function repleaceMeasureLog ({ warningConfigs, measureLog }) {
  measureLog.warningLevel = null
  // check vuot nguong va chuan bi vuot nguong
  if (measureLog && measureLog.value && measureLog.maxLimit) {
    // Check vuot nguong
    if (measureLog.value >= measureLog.maxLimit) {
      measureLog.warningLevel = warningLevels.EXCEEDED
    } else if (
      measureLog.value / measureLog.maxLimit * 100 >=
      warningConfigs.exceededPreparing.value
    ) {
      // Check chuan bi vuot
      measureLog.warningLevel = warningLevels.EXCEEDED_PREPARING
    }
  }
  return measureLog
}

module.exports = function getDataImport ({
  station,
  receivedAt,
  measureData,
  warningConfigs
}) {
  let dataImport = {
    receivedAt: receivedAt,
    measuringLogs: {}
  }

  const approve = _.get(station, 'options.approve', { allowed: false })
  let isRemove = false
  const approveLogs = { measuringLogs: {} }
  const removeLogs = { measuringLogs: {} }

  // console.log(station.measuringList, measureData)

  station.measuringList.forEach(item => {
    let measureFinded = measureData.find(
      measureSoure => {
        // console.log(measureSoure.measureName,item.unit)
        return measureSoure.measureName === item.measuringSrc
      }
    )

    // console.log(measureFinded)

    if (measureFinded) {
      const value = item.ratio ? measureFinded.value * item.ratio : measureFinded.value
      approveLogs.measuringLogs[item.measuringDes] = { approvedValue: value, hasApproved: false }
      removeLogs.measuringLogs[item.measuringDes] = { removeValue: value }

      const rules = _.get(approve, ['rules', item.measuringDes], [])
      const today = moment(receivedAt)
      if (rules.length > 0) {
        // DEVICE_CALIBRATION
        const configCalibration = _.get(approve, ['rules', item.measuringDes, 'configCalibration'])
        const dayList = _.get(configCalibration, ['dayList'], [])
        if (!isRemove && _.includes(rules, 'DEVICE_CALIBRATION') && dayList.length > 0 && _.includes(dayList, today.weekday())) {
          if (_.get(configCalibration, ['hours', 'from']) && _.get(configCalibration, ['hours', 'to'])) {
            const from = moment(_.get(configCalibration, ['hours', 'from']), 'HH:mm')
            const to = moment(_.get(configCalibration, ['hours', 'to']), 'HH:mm')
            if (_.inRange(today.unix(), from.unix(), to.unix())) {
              isRemove = true
            }
          }
        }

        if (!isRemove && _.includes(rules, 'ZERO') && _.isEqual(value, 0)) isRemove = true
        if (!isRemove && _.includes(rules, 'DEVICE_STATUS') &&
        !_.isEqual(measureFinded.statusDevice, 0)) isRemove = true
        if (!isRemove && _.includes(rules, 'NEGATIVE') && value < 0) isRemove = true
        if (!isRemove && _.includes(rules, 'OUT_RANGE')) {
          const valueRules = _.get(approve, ['valueRules', item.measuringDes], null)
          if (valueRules && valueRules.maxRange !== null && valueRules.minRange !== null &&
          !_.inRange(value, valueRules.minRange, valueRules.maxRange + 0.000001)) {
            isRemove = true
          } else if (valueRules && valueRules.minRange !== null && value < valueRules.minRange) {
            isRemove = true
          } else if (valueRules && valueRules.maxRange !== null && value > valueRules.maxRange) {
            isRemove = true
          }
        }
      }

      dataImport.measuringLogs[item.measuringDes] = repleaceMeasureLog({
        warningConfigs,
        measureLog: {
          value,
          minLimit: item.minLimit,
          maxLimit: item.maxLimit,
          statusDevice: measureFinded.statusDevice
        }
      })
    }
  })

  if (isRemove) {
    return _.merge(dataImport, removeLogs)
  } else {
    return _.merge(dataImport, approveLogs)
  }

  // return dataImport
}

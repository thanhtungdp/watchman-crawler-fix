const fs = require('fs')
const path = require('path')
const moment = require('moment')
const logger = require('logger')
const _ = require('lodash')
// const moveFile = require('./moveFile')
const importData = require('./importData')
const getDataImport = require('./getDataImport')
const readFileType = require('../readFileType')

const FILE_TYPE = readFileType.FILE_TYPE

async function changeFileName (filename) {
  let newFileName = path.join(
    path.dirname(filename),
    'IMPORTED_' + path.basename(filename)
  )
  await fs.renameSync(filename, newFileName)
  return newFileName
}

async function readAndWriteToServer ({
  filename,
  station,
  organization,
  warningConfigs,
  q
}) {
  try {
    const extensionFile = station.extensionFile || 'txt'
    // TODO: Sau sẽ lấy từ cấu hình
    const fileType = extensionFile === 'txt' ? FILE_TYPE.THONG_TU_24 : FILE_TYPE.CSV_VAG
    const { measureData, receivedAt, error, csv } = await readFileType({
      fileType,
      extensionFile,
      filename,
      stationKey: station.stationAuto,
      showLog: false
    })

    if (error) {
      return false
    }

    let res = null

    if (csv) {
      const promiseList = []
      const groupData = _.groupBy(measureData, 'key')
      // console.log(measureData)
      _.mapKeys(groupData, (value, key) => {
        promiseList.push(importData({
          organization,
          stationKey: station.stationAuto,
          data: getDataImport({
            station,
            measureData: value,
            receivedAt: moment(key, 'YYYYMMDDHHmmss').toDate(),
            warningConfigs
          })
        }))
        return key
      })
      await Promise.all(promiseList)
      res = { success: true }
    } else {
      const dataImport = getDataImport({
        station,
        measureData,
        receivedAt,
        warningConfigs
      })
      res = await importData({
        organization,
        stationKey: station.stationAuto,
        data: dataImport
      })
      // console.log(res.data.lastLog.measuringLogs)
    }

    if (res && res.success) {
      if (extensionFile === 'csv') {
        const newFilename = await changeFileName(filename)
        // moveFile({filename, type: 'csv_imported'})
        logger.info(`move -imported ${newFilename}`)
      } else {
        const newFilename = await changeFileName(filename)
        logger.info(`imported ${newFilename}`)
      }
    } else {
      logger.error(`danger ${filename} - ${res.message} `)
    }

    return res
  } catch (e) {
    logger.error(`ex ${filename} - ${e} `)
    return false
  }
}

module.exports = readAndWriteToServer
module.exports.getDataImport = getDataImport
module.exports.readAndWriteToServer = readAndWriteToServer
module.exports.changeFileName = changeFileName

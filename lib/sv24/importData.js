const dataStationAutoDao = require('../../dao/dataStationAutoDao')
const stationAutoDao = require('../../dao/stationAutoDao')
const createDataStationModel = require('../../models/DataStationAuto')
const createStationAutoModel = require('../../models/StationAuto')
const { createConnection } = require('../../shared/mongo-utils/connect')
const config = require('../../config')

module.exports = async function importData ({
  organization,
  stationKey,
  data: { receivedAt, measuringLogs }
}) {
  try {
    const mongoDbConnect = await createConnection({
      ...organization.databaseInfo,
      address: config.MONGODB_ADDRESS ? config.MONGODB_ADDRESS : organization.databaseInfo.address
    })
    const dataStationModel = createDataStationModel(stationKey, mongoDbConnect)
    const stationAutoModel = createStationAutoModel(mongoDbConnect)
    await dataStationAutoDao.createOrUpdate(
      {
        receivedAt,
        measuringLogs
      },
      dataStationModel
    )
    const resStationAuto = await stationAutoDao.updateLastLog(
      stationKey,
      { receivedAt, measuringLogs },
      stationAutoModel
    )
    return { success: true, data: resStationAuto }
  } catch (e) {
    return { error: true, message: e.message }
  }
}

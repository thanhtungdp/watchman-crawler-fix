const createStationAutoModel = require('../../models/StationAuto')
const organizationDao = require('../../dao/organizationDao')
const stationAutoDao = require('../../dao/stationAutoDao')
const { createConnection } = require('../../shared/mongo-utils/connect')
const Queue = require('better-queue')
const CategoryApi = require('../../api/CategoryApi')
const logger = require('../../logger')
const config = require('../../config')

const q = new Queue(
  async function (input, cb) {
    if (!input.length) {
      const { func, params } = input
      await func(params)
      cb(null)
    } else {
      for (let i = 0; i < input.length; i++) {
        const { func, params } = input[i]
        await func(params)
      }
      cb(null)
    }
  },
  { batchSize: 1, batchDelayTimeout: 1000, afterProcessDelay: 300 }
)

module.exports.q = q

async function getStations (organization) {
  try {
    const mongoDbConnect = await createConnection({
      ...organization.databaseInfo,
      address: config.MONGODB_ADDRESS ? config.MONGODB_ADDRESS : organization.databaseInfo.address
    })
    const stationAutoModel = createStationAutoModel(mongoDbConnect)
    const stations = await stationAutoDao.getConfiguredLogger(stationAutoModel)
    return { success: true, stations: stations }
  } catch (e) {
    logger.error('get all stations', e)
    return { error: true, message: e.message }
  }
}

async function getOneStation (stationKey, organization) {
  try {
    const mongoDbConnect = await createConnection({
      ...organization.databaseInfo,
      address: config.MONGODB_ADDRESS ? config.MONGODB_ADDRESS : organization.databaseInfo.address
    })
    const stationAutoModel = createStationAutoModel(mongoDbConnect)
    const station = await stationAutoDao.getOneConfiguredLogger(
      stationAutoModel,
      { key: stationKey }
    )
    return { success: true, stations: station }
  } catch (e) {
    logger.error('get one stations', e)
    return { error: true, message: e.message }
  }
}

async function getOrganizations () {
  const organizations = await organizationDao.findOnDemand()
  const promises = organizations.map(async organization => {
    const { success, stations } = await getStations(organization)
    return {
      organization: organization,
      stations: success ? stations : []
    }
  })
  const newOrganizations = []
  for (const promise of promises) {
    let organization = await promise
    newOrganizations.push(organization)
  }
  return newOrganizations
}

async function getSystemConfigs () {
  try {
    const configsSystem = await CategoryApi.getSysConfig()
    return configsSystem.data.stationAuto.warning
  } catch (e) {
    logger.error(`category API - ${e.message}`)
    return false
  }
}

async function getAllStations () {
  const organizations = await getOrganizations()
  const warningConfigs = await getSystemConfigs()
  if (!warningConfigs) {
    logger.error('CATEGORY-API error')
    return { error: true, message: 'Get configs system error.' }
  }
  let newStations = []
  organizations.forEach(({ organization, stations }) => {
    newStations = [
      ...newStations,
      ...stations.map(station => ({
        ...station,
        organization,
        warningConfigs
      }))
    ]
  })
  return { stations: newStations, warningConfigs }
}

module.exports.getOrganizations = getOrganizations
module.exports.getAllStations = getAllStations
module.exports.getSystemConfigs = getSystemConfigs
module.exports.getOneStation = getOneStation
module.exports.getStations = getStations

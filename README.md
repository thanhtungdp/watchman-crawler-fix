# Crawler

Crawler watching file csv, txt and import to mongodb. Build on [watchman](https://facebook.github.io/watchman)

Support for
* Linux
* Windows

# Requirement

## For Windows
* Minimum: Windows 2012 R2 Standard
* pm2 on windows

## For Linux
* [watchman](https://facebook.github.io/watchman)

# Start watching

Config .env

```bash
ROOT_FTP=/root/FTP
ROOT_CSV=/root/MT_Data
ROOT_CSV_IMPORTED=/root/MT_Imported
ROOT_FTP_ERRORS=/root/FTP_ERRORS
ROOT_FTP_IMPORTED=/root/FTP_ERRORS

# Database
MONGODB_URL=...
```

If use in `Windows Server`, let's add 

```bash
IS_WINDOW=true
```

Start service for watching

```bash
pm2 start ecosystem.config.js
```

It's will start 2 instances

* Watch file TXT
* Watch file CSV


## Note
Start for initial, if missing file
```bash
pm2 start ecosystem-initial.config.js
```


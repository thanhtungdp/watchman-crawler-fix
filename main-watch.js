const cron = require('node-cron')
const terminate = require('terminate')
const spawn = require('child_process').spawn
const exec = require('child_process').exec
const { watchmanBinaryPath } = require('./lib/watchman/makeWatchFolder')

let child = null

function addChild (typeStart = 'start-initial') {
  if (child) {
    console.log('kill', child.pid)
    terminate(child.pid)
    child = null
  }
  setTimeout(() => {
    exec(`${watchmanBinaryPath} watch-del-all`, (err, stdout) => {
      if (stdout) {
        child = spawn('npm', ['run', typeStart])
        child.stdout.on('data', function (chunk) {
          console.log(chunk.toString())
        })
        child.stderr.on('data', function (data) {
          // console.log('stderr: ' + data);
        })
        child.on('close', (code, signal) => {
          console.log(
            `child process terminated due to receipt of signal ${signal}`)
        })
      }
    })
  }, 2000)
}

module.exports = (typeStart) => {
  // Start first
  addChild(typeStart)

  // Every minute start again
  cron.schedule('* * * * *', () => {
    addChild(typeStart)
  })

  console.log('start watching ' + typeStart)
}

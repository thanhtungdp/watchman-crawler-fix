module.exports = {
  apps: [
    {
      name: 'iLotusland_watchman_FTP-initial',
      script: './index-txt.js',
      env: {
        'NODE_PATH': './',
        'TYPE': 'initial'
      }
    }, {
      name: 'iLotusland_watchman_CSV-initial',
      script: './index-csv.js',
      env: {
        'NODE_PATH': './',
        'TYPE': 'initial'
      }
    }]
}
